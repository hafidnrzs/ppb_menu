import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:ppb_menu/main.dart';
import 'package:ppb_menu/makanan.dart';
import 'package:ppb_menu/styles.dart';

class OrderPage extends StatelessWidget {
  final Makanan item;
  final int quantity;

  const OrderPage({super.key, required this.item, required this.quantity});

  String totalItem(String price, int quantity) {
    NumberFormat format = NumberFormat.decimalPattern('id');
    int intValue = format.parse(price).toInt();
    int total = intValue * quantity;

    String formattedValue = format.format(total);
    return formattedValue;
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: SafeArea(
                child: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text('Pemesanan ${item.name}', style: textOrder),
          Text('${item.price} x $quantity', style: textOrder),
          const Text('Berhasil disimpan', style: textOrder),
          const Text('Total Bayar:', style: textOrder),
          Text(
            totalItem(item.price, quantity),
            style: textOrder.copyWith(fontWeight: FontWeight.bold),
          ),
          const SizedBox(height: 16),
          ElevatedButton(
              style: buttonStyle,
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const MainApp()));
              },
              child:
                  const Text('Kembali ke menu', style: TextStyle(fontSize: 20)))
        ],
      ),
    ))));
  }
}
