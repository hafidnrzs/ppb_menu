import 'package:flutter/material.dart';

const primaryColor = Color(0xFF00B2D8);
const accentColor = Color(0xFFEC008C);

const textTitle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.white,
);
const textH1 = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
);
const textNormal = TextStyle(fontSize: 16);
const textOrder = TextStyle(fontSize: 24, height: 1.5);

var buttonStyle = ElevatedButton.styleFrom(
  padding: const EdgeInsets.all(16),
  backgroundColor: accentColor,
);
