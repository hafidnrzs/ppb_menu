import 'package:flutter/material.dart';
import 'package:ppb_menu/makanan.dart';
import 'package:ppb_menu/order_page.dart';
import 'package:ppb_menu/styles.dart';

class DetailPage extends StatefulWidget {
  final Makanan item;

  const DetailPage({super.key, required this.item});

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {
  int quantity = 1;

  void onPesanClicked() {}

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: SafeArea(
              child: Container(
        padding: const EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(12),
              child: Image.asset(
                widget.item.image,
                height: 250,
                alignment: Alignment.center,
                fit: BoxFit.cover,
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                widget.item.name,
                style: textTitle.copyWith(color: primaryColor),
                textAlign: TextAlign.start,
              ),
            ),
            Text(widget.item.desc, style: textNormal),
            AddOrder(item: widget.item, quantity: quantity),
          ],
        ),
      ))),
    );
  }
}

// ignore: must_be_immutable
class AddOrder extends StatefulWidget {
  AddOrder({
    super.key,
    required this.item,
    required this.quantity,
  });

  final Makanan item;
  int quantity;

  @override
  State<StatefulWidget> createState() =>
      // ignore: no_logic_in_create_state
      _AddOrder(item: item, quantity: quantity);
}

class _AddOrder extends State<AddOrder> {
  final Makanan item;
  int quantity;

  _AddOrder({required this.item, required this.quantity});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.symmetric(vertical: 8),
          decoration: BoxDecoration(
            border: Border.all(color: Colors.grey, width: 1),
            borderRadius: BorderRadius.circular(4),
          ),
          child: Row(
            children: [
              IconButton(
                  onPressed: () {
                    if (quantity > 1) {
                      setState(() {
                        quantity--;
                      });
                    }
                  },
                  icon: const Icon(Icons.remove)),
              Container(
                  width: 30,
                  height: 30,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 1)),
                  child: Text('$quantity', style: textNormal)),
              IconButton(
                  onPressed: () {
                    setState(() {
                      quantity++;
                    });
                  },
                  icon: const Icon(Icons.add)),
              const SizedBox(width: 32),
              Text(item.price,
                  style: textNormal.copyWith(fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        Container(
          width: double.infinity,
          child: ElevatedButton(
              style: buttonStyle,
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            OrderPage(item: widget.item, quantity: quantity)));
              },
              child: const Text('Pesan', style: TextStyle(fontSize: 20))),
        )
      ],
    );
  }
}
