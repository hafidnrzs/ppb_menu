class Makanan {
  String name;
  String shortDesc;
  String desc;
  String price;
  String image;

  Makanan({
    required this.name,
    required this.shortDesc,
    required this.desc,
    required this.price,
    required this.image,
  });

  static List<Makanan> dataDummy = [
    Makanan(
      name: 'Mie Suit',
      shortDesc:
          'Mie dengan dengan bumbu racikan rahasia, ditemani taburan ayam cincang dan pangsit goreng.',
      desc:
          'Semangkuk kelezatan mie dipadukan dengan bumbu racikan rahasia yang super nikmat. Ditemani taburan ayam cincang dan pangsit goreng yang maknyus abis.',
      price: '9.500',
      image: 'assets/mie_suit.jpg',
    ),
    Makanan(
      name: 'Mie Hompimpa',
      shortDesc:
          'Mie dengan rasa pedas asin, dengan topping ayam cincang dan kerupuk pangsit.',
      desc:
          'Semangkuk mie dengan rasa pedas asin yang bakal membuat mulut kamu meledak! Tentunya dengan topping ayam cincang dan kerupuk pangsit yang gurih.',
      price: '9.500',
      image: 'assets/mie_hompimpa.jpg',
    ),
    Makanan(
      name: 'Mie Gacoan',
      shortDesc:
          'Mie dengan rasa pedas manis. Untuk yang suka manis harus cobain yang satu ini.',
      desc:
          'Untuk yang suka manis harus cobain mie yang satu ini. Mie dengan rasa manis atau buat kamu yang ingin mencoba sensasi pedas manis.',
      price: '9.500',
      image: 'assets/mie_gacoan.jpg',
    ),
    Makanan(
      name: 'Pangsit Goreng',
      shortDesc:
          'Dimsum goreng yang berisi daging ayam cincang dan dibalut dengan kulit pangsit.',
      desc:
          'Dimsum goreng yang berisi daging ayam cincang di dalamnya dan dibalut dengan kulit pangsit di luarnya yang renyah dan kriuk abis.',
      price: '9.500',
      image: 'assets/pangsit_goreng.jpg',
    ),
    Makanan(
      name: 'Siomay',
      shortDesc:
          'Dimsum kukus isi ayam dicampur sedikit udang dibungkus dengan kulit siomay yang mantul!)',
      desc:
          'Dimsum kukus berisi ayam dan dicampur sedikit udang, kemudian dibungkus dengan kulit siomay yang mantul!',
      price: '8.600',
      image: 'assets/siomay.jpg',
    ),
    Makanan(
      name: 'Udang Keju',
      shortDesc:
          'Dimsum ini berisi potongan keju dan dibalut dengan tepung roti yang crispy.',
      desc:
          'Dimsum imut nan gendud ini berisi potongan keju di dalamnya, dibalut dengan tepung roti yang crispy dan yummy abis!',
      price: '8.600',
      image: 'assets/udang_keju.jpg',
    ),
    Makanan(
      name: 'Udang Rambutan',
      shortDesc:
          'Bola daging diselimuti oleh surai naga. Juicy di dalam, kriuk di luar.',
      desc:
          'Bola daging yang diselimuti oleh surai naga, juicy di bagian dalam dan kriuk di luarnya. Dijamin maknyus.',
      price: '8.600',
      image: 'assets/udang_rambutan.jpg',
    ),
  ];
}
