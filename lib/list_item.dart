import 'package:flutter/material.dart';
import 'package:ppb_menu/detail_page.dart';
import 'package:ppb_menu/makanan.dart';
import 'package:ppb_menu/styles.dart';

class ListItem extends StatelessWidget {
  const ListItem({
    super.key,
    required this.menu,
  });

  final Makanan menu;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          border: Border(bottom: BorderSide(color: primaryColor, width: 1.5))),
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.asset(
                menu.image,
                width: 120,
                fit: BoxFit.cover,
              )),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(menu.name, style: textH1),
                Text(menu.shortDesc),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(menu.price,
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold)),
                    ElevatedButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      DetailPage(item: menu)));
                        },
                        style: ElevatedButton.styleFrom(
                            backgroundColor: accentColor),
                        child: const Text(
                          'Pesan',
                          style: TextStyle(fontSize: 16),
                        ))
                  ],
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
