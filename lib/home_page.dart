import 'package:flutter/material.dart';
import 'package:ppb_menu/list_item.dart';
import 'package:ppb_menu/makanan.dart';
import 'package:ppb_menu/styles.dart';

class HomePage extends StatelessWidget {
  const HomePage({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    List<Makanan> listMenu = Makanan.dataDummy;

    return SafeArea(
      child: Column(
        children: [
          Container(
            alignment: Alignment.topLeft,
            color: primaryColor,
            padding: const EdgeInsets.all(12),
            child: const Text('Pilih Menu', style: textTitle),
          ),
          Expanded(
            child: ListView.builder(
                itemCount: listMenu.length,
                itemBuilder: (context, index) {
                  return ListItem(menu: listMenu[index]);
                }),
          )
        ],
      ),
    );
  }
}
